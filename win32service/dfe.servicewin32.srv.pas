unit dfe.servicewin32.srv;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Classes,
  dfe.httpserver.base,
  dfe.lib.util,
  dfe.httpserver,
  Vcl.SvcMgr,
  dfe.dao.base,
  dfe.services.websocket,
  dfe.schedule.MDE,
  ShlObj,
  Vcl.ExtCtrls

    ;

type
  TGB_DFE_SERVICE = class(TService)
    tmstart: TTimer;
    procedure tmstartTimer(Sender: TObject);
    procedure ServiceCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;

    { Public declarations }
  end;

var
  FidlistenNfe: cardinal;
  GB_DFE_SERVICE: TGB_DFE_SERVICE;
procedure SetMongoDB(Restart: Boolean = false);
procedure AutoRestart();

implementation

uses
  dfe.schedule.manutencao;
{$R *.dfm}

{ ----------------------------------------------------------------------------- }
procedure GlobalLog(Msg: string; akind: Integer = 1);

var
  strSaida: string;

  procedure registrarLogEmArquivo;
  var
    p: string;
    f: TextFile;
  begin
    p := ExtractFilePath(ParamStr(0)) + 'log\global\';
    ForceDirectories(p);
    p := p + FormatDateTime('yyyymmdd', now) + '_' +
      ChangeFileExt(ExtractFileName(ParamStr(0)), '') + '.log';
    AssignFile(f, p);
    if not FileExists(p) then
      Rewrite(f)
    else
      Append(f);
    Writeln(f, strSaida);
    Closefile(f);
  end;

begin
  try
    strSaida := FormatDateTime('hh:nn:ss.zzz', now) + Msg;
    OutputDebugString(pchar(datetimetostr(now) + ' - ' + Msg));

  except
  end;
  try
    registrarLogEmArquivo;
  except
  end;
end;

{ ----------------------------------------------------------------------------- }
procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  GB_DFE_SERVICE.Controller(CtrlCode);
end;

{ ----------------------------------------------------------------------------- }
function TGB_DFE_SERVICE.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

{ ----------------------------------------------------------------------------- }
procedure TGB_DFE_SERVICE.ServiceCreate(Sender: TObject);
begin
  tmstart.Enabled := true;
end;

procedure listenNfe();
begin
  try
    TDaoBase.SetPoolDb();
    THttpHandlerBase.create;
    With TScheduleMDE.create(true) do
    begin
      resume;
    end;
    With TScheduleManutencao.create(true) do
    begin
      resume;
    end;

    GlobalLog('[ServiceWin32] Inicializa��o  GBDFE concluida');
  except
    on E: Exception do
      GlobalLog('[ServiceWin32] 106 erro listen ' + E.Message);
  end;
end;

{ ----------------------------------------------------------------------------- }
procedure AutoRestart();
var
  LfileName: string;
  lStartUpInfo: TStartUpInfo;
  lProcesso: TProcessInformation;
  Lcomando: string;
begin
  LfileName := ExtractFilePath(GetModuleName(HInstance)) +
    'ManutencaoService.bat';
  GlobalLog('[ServiceWin32] Realizando manutencao diaria do servico ');

  /// WinExec(PAnsiChar('taskkill /IM GB_DFE_ServiceWin32.exe /F'), 0);
  with TStringList.create do
  begin
    add('net stop GB_DFE_SERVICE');
    add('ping 127.0.0.1 -n 10 > nul');
    add('taskkill /IM GB_DFE_ServiceWin32.exe /F');
    add('ping 127.0.0.1 -n 5 > nul');
    add('net start GB_DFE_SERVICE');
    savetofile(LfileName);
    free;
  end;
  Lcomando := 'cmd.exe /c ' + '"' + LfileName + '"';
  GlobalLog('[ServiceWin32]   -- Reiniciando em alguns segundos... ');
  WinExec(Pansichar(AnsiString(Lcomando)), 0);
end;

{ ----------------------------------------------------------------------------- }
procedure SetMongoDB(Restart: Boolean = false);
var
  programFiles: string;
  mongoCommand: string;
  pathMongo: string;
  pmongoCommand: string;
begin
  Try
    ForceDirectories('C:\data');
    ForceDirectories('C:\data\db');
    if not DirectoryExists('C:\data\db') then
      GlobalLog(
        '[ServiceWin32] o servi�o nao pode criar a pasta C:\data\db necessaria a execu��o do sistema');

    programFiles := GetEnvironmentVariable('ProgramW6432');
    if programFiles = '' then
      programFiles := GetSpecialFolderPath(CSIDL_PROGRAM_FILES);
    pathMongo := programFiles + '\MongoDB\Server\3.0\bin\';
    if Restart then
    begin
      GlobalLog('[ServiceWin32] Finaluzando o servidor mongo.. ');
      WinExec(Pansichar('taskkill /IM mongod.exe /F'), 0);
    end;

    GlobalLog('[ServiceWin32] Inciando mongoDB em : ' + pathMongo);

    mongoCommand := pathMongo + '\' +
      'mongod --port 27017 --logpath="c:\data\log" --install --serviceName "MongoDB"';

    with TStringList.create do
    begin
      add('"' + programFiles + '"\MongoDB\Server\3.0\bin\' +
        'mongod --port 27017 --logpath="c:\data\log" --install --serviceName "MongoDB"');

      savetofile(ExtractFilePath(GetModuleName(HInstance)) +
        'installMongo.cmd');
      free;
    end;

    if not FileExists(pathMongo + 'mongod.exe') then
    begin
      GlobalLog(
        '[ServiceWin32] executavel mongod.exe n�o localizado no diretorio ' +
        pathMongo);
    end
    else
    begin

      pmongoCommand := Pansichar(mongoCommand);
      WinExec(Pansichar(ExtractFilePath(GetModuleName(HInstance)) +
        'installMongo.cmd'), 0);
      sleep(1000);
      GlobalLog('[ServiceWin32]Iniciando mongo db');
      WinExec(Pansichar('net start MongoDB'), 0);

    end;
  except
    on E: Exception do
      GlobalLog('[ServiceWin32]o servi�o nao pode configurar a execu��o ' +
        E.Message);

  End;
end;

{ ----------------------------------------------------------------------------- }
procedure SetWebsocketServer();
begin
  GlobalLog('[ServiceWin32]Iniciando websocket na porta 30086 ');
  try
    FGbDFEWebSocket := TGbDFEWebSocket.create;
  except
    on E: Exception do
      GlobalLog('[ServiceWin32] 173 ' + E.Message);
  end;
end;

{ ----------------------------------------------------------------------------- }
procedure TGB_DFE_SERVICE.tmstartTimer(Sender: TObject);
begin
  tmstart.Enabled := false;
  try
    SetMongoDB;
    SetWebsocketServer();
    BeginThread(Nil, 0, @listenNfe, nil, 0, FidlistenNfe);

  except
    on E: Exception do
      GlobalLog('[ServiceWin32] 187 ' + E.Message);
  end;
end;

{ ----------------------------------------------------------------------------- }
end.
