unit dfe.dao.inutilizacao;

interface

Uses
  windows,
  sysutils,
  REST.JSON.Types,
  REST.JSON,
  system.JSON,
  dfe.dao.base,
  dfe.model.inutilizacao,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.MongoDBDef,
  FireDAC.Phys.MongoDB,
  FireDAC.Comp.UI,
  Data.DB,
  FireDAC.Comp.Client,
  FireDAC.Stan.param,
  FireDAC.Phys.MongoDBWrapper,
  system.JSON.Types,
  system.JSON.BSON,
  system.JSON.Builders,
  system.Rtti,
  system.JSON.Readers,
  system.Diagnostics,
  FireDAC.Stan.Util,
  dfe.lib.Util,
  classes;

type
  TDaoInutilizacao = class(TDaoBase)
  private
    function getBasicSelect: string;

    function getBasicList: String;
    procedure setFilter(qry: TFDQuery; param: TJSONObject);
    procedure setOrderBy(qry: TFDQuery);
    procedure setSqlServerDataTable(pInutilizacao: TInutilizacao);

  public
    function gravarInutilizacao(pInutilizacao: TInutilizacao): Boolean;
    function getInutilizacao(param: TJSONObject): TInutilizacao;
    function listInutilizacaos(param: TJSONObject): TInutilizacoes;

  end;

implementation

{ TDaoNfe }
{ ---------------------------------------------------------------------------- }

function TDaoInutilizacao.getBasicList: String;
begin
  result := getBasicSelect;
end;

function TDaoInutilizacao.getBasicSelect: string;
var
  tmpSql: TStringList;
begin
  tmpSql := TStringList.Create;
  try
    with tmpSql do
    begin
      Add(' Select distinct  ');
      Add('  i.codigo_loja ');
      Add(' ,i.numeroInicial   ');
      Add(' ,i.numeroFinal ');
      Add(' ,i.data_inclusao   as dhRecbto  ');
      Add(' ,i.justificativa  ');
      Add(' ,i.ano      ');
      Add(' ,i.modelo   ');
      Add(' ,i.serie    ');
      Add(' ,i.tpAmb    ');
      Add(' ,i.verAplic ');
      Add(' ,i.cstat    ');
      Add(' ,i.xmotivo  ');
      Add(' ,i.uf       ');
      Add(' ,i.cnpj     ');
      Add(' ,i.nProt as  protocolo  ');
      Add(' ,i.UsuInu   ');
      Add(' from notas_inutilizadas i  ');

    end;
  finally
    FreeAndNil(tmpSql);
  end;

end;

procedure TDaoInutilizacao.setSqlServerDataTable(pInutilizacao: TInutilizacao);
var
  qry: TFDQuery;
  astream: TStringStream;
begin
  qry := TFDQuery.Create(Nil);
  qry.Connection := FDConNFe;
  try
    with qry.SQL do
    begin
      Add('INSERT INTO              ');
      Add('  dbo.Inutilizacao       ');
      Add('(                        ');
      Add('  ano,                   ');
      Add('  Modelo,                ');
      Add('  Serie,                 ');
      Add('  justificativa,         ');
      Add('  dhRecbto,              ');
      Add('  tpAmb,                 ');
      Add('  verAplic,              ');
      Add('  cStat,                 ');
      Add('  xMotivo,               ');
      Add('  cUF,                   ');
      Add('  CNPJ,                  ');
      Add('  nProt,                 ');
      Add('  numeroInicial,         ');
      Add('  numeroFinal,           ');
      Add('  [xml]                  ');
      Add(')                        ');
      Add('VALUES (                 ');
      Add('  :ano,                  ');
      Add('  :Modelo,               ');
      Add('  :Serie,                ');
      Add('  :justificativa,        ');
      Add('  :dhRecbto,             ');
      Add('  :tpAmb,                ');
      Add('  :verAplic,             ');
      Add('  :cStat,                ');
      Add('  :xMotivo,              ');
      Add('  :cUF,                  ');
      Add('  :CNPJ,                 ');
      Add('  :nProt,                ');
      Add('  :numeroInicial,        ');
      Add('  :numeroFinal,          ');
      Add('  :xml                   ');
      Add(');                       ');

    end;
    qry.ParamByName('ano').AsInteger := pInutilizacao.ano;
    qry.ParamByName('Modelo').AsInteger := pInutilizacao.ano;
    qry.ParamByName('Serie').AsInteger := pInutilizacao.serie;;
    qry.ParamByName('justificativa').AsString := pInutilizacao.justificativa;
    qry.ParamByName('dhRecbto').AsDateTime := pInutilizacao.dhRecbto;
    qry.ParamByName('tpAmb').AsString := pInutilizacao.tpAmb;
    qry.ParamByName('verAplic').AsString := pInutilizacao.verAplic;
    qry.ParamByName('cStat').AsInteger := pInutilizacao.cstat;
    qry.ParamByName('xMotivo').AsString := pInutilizacao.xmotivo;
    qry.ParamByName('cUF').AsString := pInutilizacao.UF;
    qry.ParamByName('CNPJ').AsString := pInutilizacao.cnpj;
    qry.ParamByName('nProt').AsString := pInutilizacao.protocolo;
    qry.ParamByName('numeroInicial').AsInteger := pInutilizacao.numeroInicial;
    qry.ParamByName('numeroFinal').AsInteger := pInutilizacao.numeroFinal;

    astream := TStringStream.Create('');
    try
      astream.WriteString(pInutilizacao.xmlEvento);
      qry.Params.ParamByName('xml').LoadFromStream(astream, ftBlob);

    finally
      astream.free;
    end;

    qry.ExecSQL();
  finally
    FreeAndNil(qry)
  end;
end;

function TDaoInutilizacao.getInutilizacao(param: TJSONObject): TInutilizacao;
var
  oCrs: IMongoCursor;
  s: string;
  oInutilizacao: TInutilizacao;
  oQry: TMongoQuery;
  collection: TMongoCollection;
begin
  if Assigned(param) then
  begin
    collection := FCon.Databases[_Db].GetCollection(_ColectionInutilizacao);
    oQry := TMongoQuery.Create(collection.Env).Limit(500);
    oQry.Match(param.ToJSON);
    oCrs := collection.Find(oQry, []);
  end
  else
  begin
    collection := FCon.Databases[_Db].GetCollection(_ColectionInutilizacao);
    oQry := TMongoQuery.Create(collection.Env).Limit(500);
    oCrs := collection.Find(oQry, [])
  end;
  result := TInutilizacao.Create();
  if oCrs.Next then
  begin
    s := oCrs.Doc.AsJSON;
    result := Tjson.JsonToObject<TInutilizacao>(s);
  end;

end;

{ ---------------------------------------------------------------------------- }
function TDaoInutilizacao.gravarInutilizacao(pInutilizacao
  : TInutilizacao): Boolean;
var
  oText: string;
  oDoc: TMongoDocument;
  oCol: TMongoCollection;
begin
  if Assigned(pInutilizacao) then
  begin

    oCol := FCon[_Db][_ColectionInutilizacao];
    oText := Tjson.ObjectToJsonString(pInutilizacao);
    oDoc := FEnv.NewDoc;
    try
      oCol.BeginBulk;
      try
        oDoc.AsJSON := oText;
        oCol.Insert(oDoc);
        oCol.EndBulk;
      except
        oCol.CancelBulk;
        raise;
      end;
    finally
      oDoc.free;
    end;
  end;
  try
    setSqlServerDataTable(pInutilizacao);
  Except
    on e: exception do
      gravalog('[DAOINUTILIZACAO]' + e.Message,pInutilizacao.cnpj);

  end;
end;

{ ---------------------------------------------------------------------------- }
function TDaoInutilizacao.listInutilizacaos(param: TJSONObject): TInutilizacoes;
var
  LCrs: IMongoCursor;
  LConsulta: string;
  Lresult: string;
  LInutilizacao: TInutilizacao;
  LQry: TMongoQuery;
  LCollection: TMongoCollection;
begin
  LConsulta:=PrepareFilter('protocolo','dhRecbto', param);
  LCollection := FCon.Databases[_Db].GetCollection(_ColectionInutilizacao);
  LQry := TMongoQuery.create(LCollection.Env).Limit(500);
  if LConsulta <> '' then
    LQry.Match(LConsulta);
  LCrs := LCollection.Find(LQry, []);
  result := TInutilizacoes.create;
  while LCrs.Next do
  begin
    Lresult := LCrs.Doc.AsJSON;
    LInutilizacao := Tjson.JsonToObject<TInutilizacao>(Lresult);
    result.Add(LInutilizacao);
  end;
end;

procedure TDaoInutilizacao.setFilter(qry: TFDQuery; param: TJSONObject);

var
  notaFilter: TInutilizacao;

begin
  {
    notaFilter := Tjson.JsonToObject<TInutilizacao>(param);
    if Assigned(notaFilter) then
    begin
    if notaFilter.cnpj <> '' then
    qry.sql.Add(' and i.cnpj = ' + QuotedStr(notaFilter.cnpj));
    if notaFilter.numeroInicial > 0 then
    qry.sql.Add(' and n.nnf = ' + inttostr(notaFilter.numero));
    if notaFilter.serie <> '' then
    qry.sql.Add(' and cast( i.serie as integer)= ' + notaFilter.serie);
    if notaFilter.modelo <> '' then
    qry.sql.Add(' and i.modelo = ' + QuotedStr(notaFilter.modelo));
    if notaFilter.dataInicial > 0 then
    qry.sql.Add(' and format(i.dEmi, ''yyyy-MM-dd'') >= ' +
    QuotedStr(formatdatetime('yyyy-MM-dd', notaFilter.dataInicial)));
    if notaFilter.dataFinal > 0 then
    qry.sql.Add(' and format(i.dEmi, ''yyyy-MM-dd'')  <= ' +
    QuotedStr(formatdatetime('yyyy-MM-dd', notaFilter.dataFinal)));

    FreeAndNil(notaFilter);
    end; }
end;

procedure TDaoInutilizacao.setOrderBy(qry: TFDQuery);
begin

end;

{ ---------------------------------------------------------------------------- }
end.
