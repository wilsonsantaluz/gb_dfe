unit dfe.dao.cancelamento;

interface

Uses
  windows,
  System.DateUtils,
  sysutils,
  REST.JSON.Types,
  REST.JSON,
  System.JSON,
  dfe.dao.base,
  dfe.dao.nfe,
  dfe.dao.empresa,
  Math,
  dfe.model.empresa,

  dfe.model.cancelamento,
  dfe.model.nfe,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.MongoDBDef,
  FireDAC.Phys.MongoDB,
  FireDAC.Comp.UI,
  Data.DB,
  FireDAC.Comp.Client,
  FireDAC.Phys.MongoDBWrapper,
  System.JSON.Types,
  System.JSON.BSON,
  System.JSON.Builders,
  System.Rtti,
  System.JSON.Readers,
  System.Diagnostics,
  FireDAC.Stan.Util,
  classes;

type
  TDaoCancelamento = class(TDaoBase)
  private

  public
    function gravarCancelamento(pCancelamento: TCancelamento): Boolean;
    function getCancelamento(param: TJSONObject): TCancelamento;
    function listCancelamentos(param: TJSONObject): TCancelamentos;

    procedure testeinsert();
  end;

implementation

{ TDaoNfe }
{ ---------------------------------------------------------------------------- }
function TDaoCancelamento.getCancelamento(param: TJSONObject): TCancelamento;
var
  oCrs: IMongoCursor;
  s: string;

  oQry: TMongoQuery;
  collection: TMongoCollection;
begin

  if Assigned(param) then
  begin
    collection := FCon.Databases[_Db].GetCollection(_ColectionCancelamentos);
    oQry := TMongoQuery.create(collection.Env).Limit(500);
    oQry.Match(param.ToJSON);
    oCrs := collection.Find(oQry, []);
  end
  else
  begin
    collection := FCon.Databases[_Db].GetCollection(_ColectionCancelamentos);
    oQry := TMongoQuery.create(collection.Env).Limit(500);
    oCrs := collection.Find(oQry, [])
  end;
  result := TCancelamento.create();
  if oCrs.Next then
  begin
    s := oCrs.Doc.AsJSON;
    result := Tjson.JsonToObject<TCancelamento>(s);
  end;

end;

{ ---------------------------------------------------------------------------- }
function TDaoCancelamento.gravarCancelamento(pCancelamento
  : TCancelamento): Boolean;
var
  oText: string;
  oDoc: TMongoDocument;
  oCol: TMongoCollection;
  dao: TDaoNfe;
  daoEmpresa: TDaoEmpresa;
  nota: Tnota;
  empresa: TEmpresa;
begin

  dao := TDaoNfe.create;
  nota := Tnota.create;
  daoEmpresa := TDaoEmpresa.create;

  try
    if pCancelamento.protocoloCancelamento <> '' then
    begin
      nota.cnpj := pCancelamento.cnpj;
      empresa := daoEmpresa.getEmpresaCnpj(pCancelamento.cnpj);
      nota.codigo_loja := empresa.codigo_loja;
      nota.numero := pCancelamento.numero;
      nota.protocolo:=pCancelamento.protocoloCancelamento;
      nota.motivo := pCancelamento.xmotivo;
      nota.chave:=pCancelamento.chave;
      nota.situacao :=inttostr( pCancelamento.cstat);
      dao.atualizarStatus(nota);
    end;
  finally
    FreeAndNil(nota);
    FreeAndNil(dao);
  end;

  if Assigned(pCancelamento) then
  begin
    pCancelamento.Data := DateToISO8601(now);
    oCol := FCon[_Db][_ColectionCancelamentos];
    oText := Tjson.ObjectToJsonString(pCancelamento);
    oDoc := FEnv.NewDoc;
    try
      oCol.BeginBulk;
      try
        oDoc.AsJSON := oText;
        oCol.Insert(oDoc);
        oCol.EndBulk;
      except
        oCol.CancelBulk;
        raise;
      end;
    finally
      oDoc.Free;
    end;
    FQdata.CollectionName := _ColectionNotas;
    FQdata.DatabaseName := _Db;
    FQdata.QMatch := '{"chave": "' + pCancelamento.chave + '"}';
    FQdata.Open;
    if FQdata.RecordCount > 0 then
    begin
      FQdata.edit;
      FQdata.FieldByName('cancelada').AsBoolean := true;
      FQdata.Post;
    end;
  end;
end;

{ ---------------------------------------------------------------------------- }
function TDaoCancelamento.listCancelamentos(param: TJSONObject): TCancelamentos;
var
  LCrs: IMongoCursor;
  LConsulta: string;
  Lresult: string;
  LCancelamento: TCancelamento;
  LQry: TMongoQuery;
  LCollection: TMongoCollection;
begin
  LConsulta := PrepareFilter('protocoloCancelamento', 'data', param);
  LCollection := FCon.Databases[_Db].GetCollection(_ColectionCancelamentos);
  LQry := TMongoQuery.create(LCollection.Env).Limit(500);
  if LConsulta <> '' then
    LQry.Match(LConsulta);
  LCrs := LCollection.Find(LQry, []);
  result := TCancelamentos.create;
  while LCrs.Next do
  begin
    Lresult := LCrs.Doc.AsJSON;
    LCancelamento := Tjson.JsonToObject<TCancelamento>(Lresult);
    result.Add(LCancelamento);
  end;
end;

procedure TDaoCancelamento.testeinsert;
var

  oText: string;
  oDoc: TMongoDocument;
  oCol: TMongoCollection;

  pCancelamento: TCancelamento;
begin
  Randomize;
  pCancelamento := TCancelamento.create;
  pCancelamento.cnpj := '33631727100';
  pCancelamento.numero := RandomRange(1, 9999);
  pCancelamento.serie := 1;
  pCancelamento.chave := '2210' + inttostr(RandomRange(11111, 99999)) +
    '00000000000';
  pCancelamento.cstat := 135;

  if Assigned(pCancelamento) then
  begin
    oCol := FCon[_Db][_ColectionCancelamentos];
    oText := Tjson.ObjectToJsonString(pCancelamento);
    oDoc := FEnv.NewDoc;
    try
      oCol.BeginBulk;
      try
        oDoc.AsJSON := oText;
        oCol.Insert(oDoc);
        oCol.EndBulk;
      except
        oCol.CancelBulk;
        raise;
      end;
    finally
      oDoc.Free;
    end;

  end;

end;

{ ---------------------------------------------------------------------------- }
end.
