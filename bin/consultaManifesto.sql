SELECT top 1000 
  Codigo_loja,                                                
  Chave_nfe,                                                  
  Evento,                                                     
  tpEvento,                                                   
  nSeqEvento,                                                 
  desEvento,                                                  
  xJust,                                                      
  cStat,                                                      
  xMotivo,                                                    
  nProt,                                                      
  NSU,                                                        
  xNome,                                                      
  cnpj_cpf,                                                   
  IE,                                                         
  dEmi as dEmi,                  
  tpNF,                                                       
  vNF,                                                        
  digVal,                                                     
  cSitNFe,                                                    
  cSitConf,                                                   
  dhEvento as dhEvento,         
  dhRecbto as dhRecbto,         
  dhRegEvento   as hRegEvento  
FROM 
  nfe_MDFe 
WHERE 
  codigo_loja is not null 
 and format(dhEvento, 'yyyy-MM-dd') >= '2023-11-15'
 and format(dhEvento, 'yyyy-MM-dd')  <= '2023-12-05'
  order by dhevento desc 